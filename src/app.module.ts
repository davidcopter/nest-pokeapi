import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { AppService } from './app.service';
import { PokeModule } from './poke/poke.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/poke', {
      useFindAndModify: false,
    }),
    PokeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
