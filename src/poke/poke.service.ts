import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Poke, PokeDocument } from './schema/poke.schema';
import { CreatePokeDto } from './dto/create-poke.dto';
import { UpdatePokeDto } from './dto/update-poke.dto';

@Injectable()
export class PokeService {
  constructor(@InjectModel(Poke.name) private pokeModel: Model<PokeDocument>) {}

  async create(createPokeDto: CreatePokeDto): Promise<Poke> {
    const createdPoke = new this.pokeModel(createPokeDto);
    return createdPoke.save();
  }

  async findAll(): Promise<Poke[]> {
    return this.pokeModel.find().exec();
  }

  findOne(id: number) {
    return `This action returns a #${id} poke`;
  }

  async update(id: string, updatePokeDto: UpdatePokeDto): Promise<Poke> {
    return this.pokeModel.findByIdAndUpdate(id, updatePokeDto, { new: true });
  }

  remove(id: number) {
    return `This action removes a #${id} poke`;
  }
}
