export class CreatePokeDto {
  name: string;
  type: string;
  total: number;
  legendary: boolean;
}
