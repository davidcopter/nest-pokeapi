import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { PokeService } from './poke.service';
import { PokeController } from './poke.controller';
import { Poke, PokeSchema } from './schema/poke.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Poke.name, schema: PokeSchema }]),
  ],
  controllers: [PokeController],
  providers: [PokeService],
})
export class PokeModule {}
