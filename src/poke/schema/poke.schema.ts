import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PokeDocument = Poke & Document;

@Schema()
export class Poke {
  @Prop()
  name: string;

  @Prop()
  type: string;

  @Prop()
  total: number;

  @Prop()
  legendary: boolean;
}

export const PokeSchema = SchemaFactory.createForClass(Poke);
